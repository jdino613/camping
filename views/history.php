<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Order History";
	}

	function get_body_contents(){
		require "../controllers/connection.php";
?>

	<h1 class="text-center py-5">History of Your Terrible Financial Decisions</h1>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Order Id</th>
			<!-- 	<th>Image</th> -->
				<th>Items</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$userId = $_SESSION['user']['id'];
				$order_query = "SELECT * FROM orders WHERE user_id = $userId";
				//  item_order.id, items.imgPath, items.name, orders.total FROM item_order JOIN items ON (items.id = item_order.id) JOIN orders on (orders.id= item_order.order_id)";
				// $history = mysqli_query($conn, $history_query);
				$orders = mysqli_query($conn, $order_query);

				foreach ($orders as $indiv_order){
			 ?>
			 <tr>
			 	<td><?php echo $indiv_order['id'] ?></td>
			 	<td>
			 		<?php
			 		$orderId = $indiv_order['id'];
			 		$items_query = "SELECT * FROM items JOIN item_order ON (items.id = item_order.item_id) WHERE item_order.order_id = $orderId";

			 		$items = mysqli_query($conn, $items_query);

			 		foreach ($items as $indiv_item) {
			 ?>	

			 	<span><?php echo $indiv_item['name'] ?></span><br>
			 <?php
			 		}
			 ?>
			 	</td>
			 	<td><?php echo $indiv_order['total'] ?></td>
			</tr>	
			<?php 
				}
			 ?>
		</tbody>
	</table>
<?php
	}
 ?>