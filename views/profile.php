<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Profile";
	}

	function get_body_contents(){
		require "../controllers/connection.php";

 ?>

 	<h1 class="text-center py-3">Profile Page</h1>

 	<div class="container">
 		<div class="row">
 			<div class="col-lg-6">
 				<h3><?php echo $_SESSION['user']['firstName'] ?>'s Details</h3>
 					<ul>
 						<?php 
	 						$userId = $_SESSION['user']['id'];
	 						$profile_query = "SELECT * FROM contacts WHERE id = $userId";
	 						$profiles = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));
 						?>
	 					
 					</ul>
 					<table class="table tabl-striped table-bordered">
 						<thead>
 							<tr class="text-center">
 								<th>ID</th>
 								<th>First Name</th>
 								<th>Last Name</th>
 								<th>Email</th>
 							</tr>
 							<tbody>
 								<tr>
 									<td><?php echo $_SESSION['user']['id'] ?></td>
 									<td><?php echo $_SESSION['user']['firstName'] ?></td>
 									<td><?php echo $_SESSION['user']['lastName'] ?></td>
 									<td><?php echo $_SESSION['user']['email'] ?></td>
 								</tr>
 							</tbody>
 						</thead>
 					</table>
 					

 				<!-- Profile Details -->
 			</div>
 			<div class="col-lg-6">
 					<h3>Addresses:</h3>
 					<ul>
 						<?php 
 						$userId = $_SESSION['user']['id'];
 						$address_query = "SELECT * FROM addresses WHERE user_id = $userId";
 						$addresses = mysqli_query($conn, $address_query);

 						foreach($addresses as $indiv_address){
 						 ?>
 						<li>
 							<?php echo $indiv_address['address1'] . ", " . $indiv_address['address2']. "<br>" . $indiv_address['city'] . " " . $indiv_address['zipCode'] ?>
 						</li>
 						
 						<?php 
 						}
 						?>

 					</ul>
 					<form action="../controllers/add-address-process.php" method="POST">
 						<div class="form-group">
 							<label for="address1">Address 1:</label>
 							<input type="text" name="address1" class="form-control">
 						</div>
 						<div class="form-group">
 							<label for="address2">Address 2:</label>
 							<input type="text" name="address2" class="form-control">
 						</div>
 						<div class="form-group">
 							<label for="city">City:</label>
 							<input type="text" name="city" class="form-control">
 						</div>
 						<div class="form-group">
 							<label for="zipCode">ZipCode:</label>
 							<input type="text" name="zipCode" class="form-control">
 						</div>
 							<input type="hidden" name="user_id" value="<?php echo $userId ?>">
 							<button class="btn btn-secondary" type="submit">Add Address</button>
 					</form>
 					<!-- Contacts -->
 					<hr class="my-3 border-white">
 					<h3>Contacts</h3>
 					<ul>
 						<?php 
	 						$userId = $_SESSION['user']['id'];
	 						$contact_query = "SELECT * FROM contacts WHERE user_id = $userId";
	 						$contacts = mysqli_query($conn, $contact_query);

	 						foreach ($contacts as $indiv_contacts){
 						?>
	 						<li>
	 							<?php echo $indiv_contacts['contactNO'] ?>
	 						</li>
 						<?php 
 						}
 						?>
 					</ul>
 					<form action="../controllers/add-contact-process.php" method="POST">
 						<div class="form-group">
 							<label for="contactNO">Contact Number:</label>
 							<input type="text" name="contactNO" class="form-control">
 						</div>
 							<input type="hidden" name="user_id" value="<?php echo $userId ?>">
 							<button class="btn btn-info" type="submit">Add Contact</button>
 					</form>
 				</div>
 			</div>
 		</div>
 	<?php 
 	}
 	?>